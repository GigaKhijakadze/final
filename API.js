import { domen } from "./config.js"

const get = async (url) => {
        const request = await fetch(domen + url)
        const result = await request.json()
        return result
}
export const getProducts = async (sortValue) => {
        const prod = await get(`products?sort=` + sortValue) 
        return prod
}
