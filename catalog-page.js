import {getProducts} from "./API.js"
const sort = document.getElementById('sort')
const sortValue = sort.value
const productList = await getProducts(sortValue)
const catalog = document.getElementsByClassName('catalog')[0]
const catalogGener = async (productList) =>{ 
        let catalogHtml = ""
        for (let i = 0; i < productList.length; i++){
                catalogHtml += `
                        <div class="catalog__prod">
                                <div class="catalog__img">
                                       <img src="${productList[i].image}" alt="">
                                </div>
                                <div class="catalog__title">
                                        ${productList[i].title}
                                </div>
                                <p class="catalog__price">
                                        ${productList[i].price}$
                                </p>
                        </div>
        ` }
        catalog.innerHTML = catalogHtml
}
catalogGener(productList)
//--------sort
sort.addEventListener( 'change', async () =>{
        let sortValue = sort.value
        let sortedProductList = await getProducts(sortValue)
        catalogGener(sortedProductList)
})

//------------search
const searchButton = document.getElementById('searchButton')
const searchInput = document.getElementById('searchInput')

searchButton.addEventListener("click",  () => {
         let searchArr = []
         for(let i = 0; i < productList.length; i++){
                if(productList[i].title.includes(searchInput.value)){
                       searchArr[i] = productList[i] 
                }
         }
         catalogGener(searchArr)
})


